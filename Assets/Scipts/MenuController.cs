﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 
using System; 

public class MenuController : MonoBehaviour {

	public bool reset; 

	public Button startButton; 
	public Button hsButton;
	public Button doneButton;
	public Button settingsDoneButton;
	//public Button settingsButton;
	public Button shopButton; 
	public Image checkMarkImage; 
	public Image poopImage; 
	public InputField inputField;
	public InputField settingsInputField; 

	private static MenuController instance; 

	public static MenuController Instance {
		get{ return instance; }
	}
	void Awake() {

		//////////////////////////////////
		/*
		if(reset) {

		Debug.Log(DateTime.Today); 
		PlayerPrefs.DeleteKey ("Date"); 
		PlayerPrefs.DeleteKey ("Ads"); 
		PlayerPrefs.SetInt("Hearts", 0); 
		PlayerPrefs.SetString ("Name", "null"); 
		}*/
		////////////////////////////////

		if (instance == null) {
			instance = this; 
		} else if (instance != null) {
			Destroy (gameObject); 
			return; 
		}
		inputField.characterLimit = 15; 
		settingsInputField.characterLimit = 15; 

		/*
		//First start
		if (PlayerPrefs.GetString ("Name", "null").Equals("null")) {
			inputField.gameObject.SetActive (true); 
			startButton.gameObject.SetActive (false); 
			hsButton.gameObject.SetActive (false); 
			poopImage.gameObject.SetActive (false); 
			settingsButton.gameObject.SetActive (false); 
			shopButton.gameObject.SetActive (false); 
			PlayerPrefs.SetInt ("Hearts", 5); 
		}
		else{
			Debug.Log(PlayerPrefs.GetString ("Name"));
			}
		*/

	}

	void Update() {
		if (inputField.text.Length > 2) {
			doneButton.interactable = true; 
		} else if (settingsInputField.text.Length > 2) {
			settingsDoneButton.interactable = true; 
		} else {
			doneButton.interactable = false; 
			settingsDoneButton.interactable = false; 
		}
	}

	public void InputDone() {
		if (inputField.gameObject.activeSelf) {
			PlayerPrefs.SetString ("Name", inputField.text);
		} else if (settingsInputField.gameObject.activeSelf) {
			PlayerPrefs.SetString ("Name", settingsInputField.text);
			checkMarkImage.gameObject.SetActive (true);

		}
		Debug.Log ("Saved name: " + PlayerPrefs.GetString ("Name")); 
		inputField.gameObject.SetActive (false); 
		startButton.gameObject.SetActive (true); 
		hsButton.gameObject.SetActive (true); 
		poopImage.gameObject.SetActive (true); 
		//settingsButton.gameObject.SetActive (true);
		shopButton.gameObject.SetActive (true); 
	}

	public void Reset() {
		PlayerPrefs.DeleteKey ("Date"); 
		PlayerPrefs.DeleteKey ("Ads"); 
		PlayerPrefs.SetInt ("Hearts", 0); 

	}





}
