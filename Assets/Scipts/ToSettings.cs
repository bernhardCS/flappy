﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 
using UnityEngine.EventSystems;

public class ToSettings : MonoBehaviour, IPointerUpHandler {

	public Canvas menuCanvas; 
	public Canvas settingsCanvas; 
	public Text settingsinputPlaceholder; 
	public Image checkmarkImage; 

	public void OnPointerUp(PointerEventData eventData) {
		if (AudioSourceScript.Instance.Playing) {
			AudioSourceScript.Instance.PlaySound (3); 
		}
		menuCanvas.gameObject.SetActive (false); 
		settingsCanvas.gameObject.SetActive (true); 
		settingsinputPlaceholder.text = PlayerPrefs.GetString ("Name"); 
		checkmarkImage.gameObject.SetActive (false); 
	}
}
