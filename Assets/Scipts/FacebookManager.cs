﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Facebook.Unity;
using UnityEngine.UI; 
using System.Collections.Generic; 
using System;
using System.Linq;

public class FacebookManager : MonoBehaviour {

	public Text heartText; 
	public Text pannerText; 
	public Button fbButton; 

	void Awake() {
		if (!FB.IsInitialized) {
			FB.Init (); 
		} else {
			FB.ActivateApp (); 
		}
	}
	/*
	public void LoginResult() {
		FB.LogInWithReadPermissions (callback:OnLogIn); 

	}

	private void OnLogIn(ILoginResult result){
		if (FB.IsLoggedIn) {
			AccessToken token = AccessToken.CurrentAccessToken; 
			userIDText.text = token.UserId; 
		} else {
			Debug.Log ("Canceled Login"); 
		}
	}*/

	public void Share(){
		if (!(PlayerPrefs.GetString ("Date").Equals (DateTime.Today.ToString ()))) {

			if (fbButton.interactable) {
				// to allouw only one share per day
				PlayerPrefs.SetString ("Date", DateTime.Today.ToString ()); 
				FB.ShareLink (
					new Uri ("https://www.facebook.com/Flappy-Poop-1438119749614113/"),
					"Flappy Poop!",
					"I just played Flappy Poop!",
					new Uri ("https://scontent-vie1-1.xx.fbcdn.net/v/t31.0-8/21688315_1438120036280751_4881403489383321961_o.png?oh=261b87ae313f47023d74f7218b60f38a&oe=5A513A65"),
					callback: OnShare);
			}
		} else {
			//StartCoroutine (ErrorText ()); 
			String helper = pannerText.text; 
			pannerText.text = "You can share \n only once per day!"; 
			pannerText.transform.localPosition = new Vector3 (0, 79.4F, 0); 

		}
	}


	private void OnShare(IShareResult result) {
		if (result.Cancelled || !string.IsNullOrEmpty (result.Error)) {
			Debug.Log ("ShareLink error: " + result.Error); 
			Debug.Log ("FB Error"); 
		} else if (!string.IsNullOrEmpty (result.PostId)) {
			Debug.Log (result.PostId); 
			int hearts = PlayerPrefs.GetInt ("Hearts"); 
			hearts = hearts + 3;
			PlayerPrefs.SetInt ("Hearts", hearts); 
			heartText.text = hearts.ToString ();
			fbButton.interactable = false;  

		} else {
			Debug.Log ("Share succeded");
			int hearts = PlayerPrefs.GetInt ("Hearts"); 
			hearts = hearts + 3;
			PlayerPrefs.SetInt ("Hearts", hearts); 
			heartText.text = hearts.ToString ();
			fbButton.interactable = false;  

		}
	}
		

	


}
