﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColumnPool : MonoBehaviour {

	public int columnPoolSize = 5; 
	public GameObject prefab; 
	public float spawnRate = 4f; 
	public float columnMin = -1f; 
	public float columnMax = 3.5f; 

	private GameObject[] columns; 
	private Vector2 objectPosition = new Vector2(-15f, -25f); 
	private float timeSinceLastSpawned = 3; 
	private float spawnXPos = 10f; 
	private int currentCol = 0; 

	private static ColumnPool instance; 

	public static ColumnPool Instance {
		get{ return instance; }
	}




	// Use this for initialization
	void Start () {

		if (instance == null) {
			instance = this; 
		} else if (instance != this) {
			Destroy (gameObject); 
			return;
		}
		columns = new GameObject[columnPoolSize]; 
		for (int i = 0; i < columnPoolSize; i++) {
			columns [i] = (GameObject)Instantiate (prefab, objectPosition, Quaternion.identity); 
		}
	}
	
	// Update is called once per frame
	void Update () {
		timeSinceLastSpawned += Time.deltaTime; 

		if (!Controller.instance.gameOver && timeSinceLastSpawned >= spawnRate) {
			//Controller.instance.IncColOnScreen (); 
			timeSinceLastSpawned = 0; 
			float spawnYPos = Random.Range (columnMin, columnMax); 
			columns [currentCol].transform.position = new Vector2 (spawnXPos, spawnYPos); 
			Renderer[] renderer = columns [currentCol].GetComponentsInChildren<Renderer> ();
			BoxCollider2D [] collider = columns [currentCol].gameObject.GetComponentsInChildren<BoxCollider2D> (); 
			for (int i = 0; i < renderer.Length; i++) {
				renderer [i].enabled = true; 
				collider [i].enabled = true; 
			}
			if (currentCol < columnPoolSize-1) {
				currentCol++; 
			} else {
				currentCol = 0; 
			}
		}

	}

	/*public void ResetCol(int number) {

		Renderer[] renderer = columns [number].gameObject.GetComponentsInChildren<Renderer> (); 
		BoxCollider2D [] collider = columns [number].gameObject.GetComponentsInChildren<BoxCollider2D> (); 
		for (int i = 0; i < renderer.Length; i++) {
			renderer [i].enabled = false;
			collider [i].enabled = false; 
		}
			
	}*/
}
