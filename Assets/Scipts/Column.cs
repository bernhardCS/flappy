﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Column : MonoBehaviour {



	private void OnTriggerEnter2D(Collider2D collider) {
		if (collider.GetComponent<Bird> () != null) {
			Controller.instance.BirdScored (); 
			gameObject.GetComponent<Renderer>().enabled = false; 
			gameObject.GetComponent<BoxCollider2D> ().enabled = false; 
			if (AudioSourceScript.Instance.Playing) {
				gameObject.GetComponent<AudioSource> ().Play (); 
			}
		}
	}
}
