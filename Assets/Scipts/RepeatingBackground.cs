﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RepeatingBackground : MonoBehaviour {

	private BoxCollider2D bc; 
	private float groundHorzontalLength; 

	// Use this for initialization
	void Start () {
		bc = GetComponent<BoxCollider2D>(); 
		groundHorzontalLength = bc.size.x; 
	}
	
	// Update is called once per frame
	void Update () {
		if (transform.position.x < -groundHorzontalLength) {
			reposition(); 
		}
	}

	private void reposition() {
		Vector2 groundOffset = new Vector2 (groundHorzontalLength * 2f, 0); 
		transform.position = (Vector2)transform.position + groundOffset; 
	}
}
