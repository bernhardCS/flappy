﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class InputDone : MonoBehaviour, IPointerUpHandler {

	public void OnPointerUp(PointerEventData eventData) {
		if (gameObject.GetComponent<Button> ().interactable) {
			if (AudioSourceScript.Instance.Playing) {
				AudioSourceScript.Instance.PlaySound (3); 
			}
			MenuController.Instance.InputDone (); 
		}
	}

}
