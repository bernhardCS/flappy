﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Bird : MonoBehaviour{


	public float upForce = 200f; 
	public GameObject tapToStartText; 
	public float tiltSmooth; 

	private Quaternion downRotation; 
	private Quaternion forwardRotation; 
	private bool isDead = false; 
	private bool invincible = false; 
	private Rigidbody2D rb; 
	private Animator anim; 
	private Vector3 startPosition;

	private static Bird instance; 

	public static Bird Instance {
		get{ return instance; }
	}




	// Use this for initialization
	void Start () {

		if (instance == null) {
			instance = this; 
		} else if (instance != this) {
			Destroy (gameObject); 
			return;
		}
		rb = GetComponent<Rigidbody2D>(); 
		anim = GetComponent<Animator> (); 
		downRotation = Quaternion.Euler (0, 0, -90); 
		forwardRotation = Quaternion.Euler (0, 0, 35); 
		tiltSmooth = 0.5F; 
		startPosition = new Vector3 (-1, 0, 0); 

	}
	
	// Update is called once per frame

	void Update () {
		if (!isDead) {
			if (Input.GetMouseButtonDown (0)) {
				if (tapToStartText != null) {
					Destroy (tapToStartText);

				}
				if (invincible) {
					invincible = false; 
					StartCoroutine (PoopInvincible ()); 
				}
				Time.timeScale = 1; 
				transform.rotation = forwardRotation; 
				rb.velocity = Vector2.zero;  
				rb.AddForce(Vector2.up * upForce); 
				gameObject.transform.position = new Vector2(gameObject.transform.position.x, 
					Mathf.Clamp(gameObject.transform.position.y,-3, 5.5F)); 
				anim.SetTrigger ("Flap"); 
			}
			transform.rotation = Quaternion.Lerp (transform.rotation, downRotation, tiltSmooth * Time.deltaTime);
		}
	}



	private void OnTriggerEnter2D(Collider2D collider) {
		if (collider.tag.Equals ("Water")) {
			gameObject.GetComponent<Renderer> ().enabled = false; 
			gameObject.GetComponent<CircleCollider2D> ().enabled = false; 
			isDead = true;
			anim.SetTrigger ("Die"); 
			Controller.instance.BirdDied ();
			rb.velocity = Vector2.zero; //so that the bird stopps slipping to the left 
			StartCoroutine (StopGame()); 
		}
	}

	private void OnCollisionEnter2D() {
		gameObject.GetComponent<CircleCollider2D> ().enabled = false; 
		isDead = true;
		anim.SetTrigger ("Die"); 
		Controller.instance.BirdDied ();
		rb.velocity = Vector2.zero; //so that the bird stopps slipping to the left 
		StartCoroutine (StopGame()); 
	}

	IEnumerator StopGame() {
		yield return new WaitUntil (() => anim.GetCurrentAnimatorStateInfo (0).tagHash == Animator.StringToHash ("Die")); 
		Time.timeScale = 0; 
	}

	public void ReAnimateBird() {
		transform.SetPositionAndRotation (startPosition, Quaternion.identity); 
		gameObject.GetComponent<Renderer> ().enabled = true; 
		invincible = true; 
		gameObject.transform.rotation = Quaternion.identity; 
		rb.velocity = Vector2.zero; 
		rb.angularVelocity = 0; 
		isDead = false; 
		anim.SetTrigger ("ReAnimate"); 
	}

	IEnumerator PoopInvincible() {
		gameObject.GetComponent<Renderer> ().enabled = false;
		yield return new WaitForSeconds (0.2F); 
		gameObject.GetComponent<Renderer> ().enabled = true;
		yield return new WaitForSeconds (0.2F); 
		gameObject.GetComponent<Renderer> ().enabled = false;
		yield return new WaitForSeconds (0.2F); 
		gameObject.GetComponent<Renderer> ().enabled = true;
		yield return new WaitForSeconds (0.2F); 
		gameObject.GetComponent<Renderer> ().enabled = false;
		yield return new WaitForSeconds (0.2F); 
		gameObject.GetComponent<Renderer> ().enabled = true;
		gameObject.GetComponent<CircleCollider2D> ().enabled = true; 
	}

}
