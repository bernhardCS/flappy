﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class RestartGame : MonoBehaviour, IPointerUpHandler{



	public void OnPointerUp(PointerEventData eventData) {
		if (Controller.instance.gameOver) {
			Time.timeScale = 1; 
			if (AudioSourceScript.Instance.Playing) {
				AudioSourceScript.Instance.PlaySound (3); 
			}
			SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex); 
		}
	}
}
