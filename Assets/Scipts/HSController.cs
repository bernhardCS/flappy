﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System; 

public class HSController : MonoBehaviour {

	private string secretKey = "61946294528"; 
	string addScoreURL = "spaceshooter.bplaced.net/addscoreflappy.php?"; //be sure to add a ? to your url
	string highscoreURL = "spaceshooter.bplaced.net/displayHighscoreFlappy.php?";							//? from me

	public string[] onlineHighscore;

	private string deviceID;
	private string name3;
	private int score;
	private string uniqueID; 


	private static HSController instance6;

	public static HSController Instance{
		get { return instance6; }
	}

	void Awake() {

		DontDestroyOnLoad (gameObject);
		// If no Player ever existed, we are it.
		if (instance6 == null) {
			instance6 = this;
			// If one already exist, it's because it came from another level.

		}
		else if (instance6 != this) {
			Destroy (gameObject);
			return;
		}
		deviceID = SystemInfo.deviceUniqueIdentifier; 

	}

	public void startGetScores() {
		StartCoroutine(GetScores());
	}

	public void startPostScores() {	
		StartCoroutine(PostScores());
	}


	public void UpdateOnlineHighscoreData(string name, int score) {
		// uniqueID,name3 and score will get the actual value before posting score
		this.uniqueID = getUniqueID();  
		this.name3 = name;
		this.score = score;

		//post it online
		startPostScores ();
	}

	public  string Md5Sum(string strToEncrypt) {
		System.Text.UTF8Encoding ue = new System.Text.UTF8Encoding();
		byte[] bytes = ue.GetBytes(strToEncrypt);

		// encrypt bytes
		System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
		byte[] hashBytes = md5.ComputeHash(bytes);

		// Convert the encrypted bytes back to a string (base 16)
		string hashString = "";

		for (int i = 0; i < hashBytes.Length; i++) {
			hashString += System.Convert.ToString(hashBytes[i], 16).PadLeft(2, '0');
		}

		return hashString.PadLeft(32, '0');
	}


	IEnumerator PostScores() {
		//This connects to a server side php script that will add the name and score to a MySQL DB.
		// Supply it with a string representing the players name and the players score.
		Debug.Log ("Device ID " + uniqueID);
		string hash = Md5Sum(name3 + score + secretKey);
		string post_url = addScoreURL + "&uniqueID=" + uniqueID + "&name=" + 
			WWW.EscapeURL (name3) + "&score=" + score + "&hash=" + hash;

		// Post the URL to the site and create a download object to get the result.
		WWW hs_post = new WWW("http://"+post_url);
		yield return hs_post; // Wait until the download is done

		if (hs_post.error != null) {
			print("There was an error posting the high score: " + hs_post.error);
		}
	}

	// Get the scores from the MySQL DB to display in a GUIText.
	IEnumerator GetScores() {

		ScrollList.Instance.loading = true;

		WWW hs_get = new WWW("http://"+highscoreURL);

		yield return hs_get;

		if (hs_get.error != null) {
			Debug.Log("There was an error getting the high score: " + hs_get.error);
		}else{
			//Change .text into string to use Substring and Split
			string help = hs_get.text;
			onlineHighscore  = help.Split(";"[0]);
		}
		ScrollList.Instance.loading = false;
		ScrollList.Instance.getScrollEntrys ();
	}

	public string getUniqueID() {


		string date = DateTime.Now.Year +"-"+DateTime.Now.Month +"-"+DateTime.Now.Day +"-"+DateTime.Now.Hour 
			+"-"+DateTime.Now.Minute +"-"+ DateTime.Now.Second; 


		uniqueID = deviceID + "!" + date; 
		return uniqueID;
	}

}

