﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoundCounter : MonoBehaviour {



	private static RoundCounter instance; 

	public static RoundCounter Instance {
		get{ return instance; }
	}

	private int round; 
	private int adRate; 


	// Use this for initialization
	void Awake () {
		if (instance == null) {
			instance = this; 
		} else if (instance != this) {
			Destroy (gameObject); 
			return;
		}
		DontDestroyOnLoad (gameObject);
		round = 0; 
		NewAdRate (); 
	}


	public bool AdRound() {
		round++; 
		return (round == adRate); 
	}

	public void NewAdRate() {
		adRate = Random.Range (4, 6);
		round = 0; 
		Debug.Log("AdRate: " + adRate); 
	}
	

}
