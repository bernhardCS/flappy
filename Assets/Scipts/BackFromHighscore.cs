﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems; 

public class BackFromHighscore : MonoBehaviour, IPointerUpHandler{

	public Canvas menuCanvas; 
	public Canvas highscoreCanvas; 
	public Canvas settingsCanvas; 

	public void OnPointerUp(PointerEventData eventData) {
		if (AudioSourceScript.Instance.Playing) {
			AudioSourceScript.Instance.PlaySound (3); 
		}
		highscoreCanvas.gameObject.SetActive (false); 
		settingsCanvas.gameObject.SetActive (false); 
		menuCanvas.gameObject.SetActive (true); 
	}

}
