﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 
using UnityEngine.EventSystems; 

public class SoundSetting : MonoBehaviour, IPointerUpHandler {

	public Sprite soundSprite; 
	public Sprite noSoundSprite; 

	private Image currentImage; 
	private bool sound = true; 

	void Start() {
		currentImage = GetComponent<Image> (); 

		if (!AudioSourceScript.Instance.Playing) {
			currentImage.overrideSprite = noSoundSprite; 
			sound = false; 
		}
			

	}

	public void OnPointerUp(PointerEventData eventdata) {

		if (sound) {
			currentImage.overrideSprite = noSoundSprite; 
			AudioSourceScript.Instance.PlayPause (false); 
			sound = false; 
		} else {
			currentImage.overrideSprite = soundSprite; 
			AudioSourceScript.Instance.PlayPause (true); 
			sound = true; 
		}



	}

}
