﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI; 
//using UnityEngine.Advertisements;  

public class Controller : MonoBehaviour {


	public static Controller instance; 
	public GameObject gameOverText; 
	public bool gameOver = false;  
	public float scrollSpeed = -1.5f; 
	public Text scoreText; 
	public Text gameOverScore; 
	public Text gameOverBest;
	public Text heartText; 
	public Text restartFromHereText; 
	public Button adButton;


	private bool secondChance = false; 
	private int score = 0; 
	private bool playAds = true; 



	// Use this for initialization
	void Awake () {
		
		if (instance == null) {
			instance = this;
		} else if (instance != null) {
			Destroy (gameObject); 
		}
		Time.timeScale = 0.0001F; 
		playAds = (!PlayerPrefs.GetString ("Ads").Equals ("No")); 
		Debug.Log("Play Ads: " + playAds); 

	}


	public void BirdDied() {
		if (AudioSourceScript.Instance.Playing) {
			AudioSourceScript.Instance.PlaySound (2); 
		}

		if (RoundCounter.Instance.AdRound () && playAds) {
			//Advertisement.Show (); 
			RoundCounter.Instance.NewAdRate (); 
		}

		gameOverText.SetActive (true); 
		int currentBest = PlayerPrefs.GetInt ("Best");
		PlayerPrefs.SetInt ("Best", Mathf.Max (currentBest, score)); 
		gameOverScore.text = "score: " + score; 
		gameOverBest.text = "best: " + PlayerPrefs.GetInt ("Best");
		scoreText.gameObject.SetActive (false); 
		/*
		if (score > 4) {
			HSController.Instance.UpdateOnlineHighscoreData (PlayerPrefs.GetString ("Name"), score); 
		}*/
	
		restartFromHereText.text = "restart from here!"; 
		restartFromHereText.transform.localPosition = new Vector3 (0, 59.4F, 0); 
		heartText.text = PlayerPrefs.GetInt ("Hearts").ToString(); 
		secondChance = true; 
	
		gameOver = true; 
	}

	public void BirdScored() {
		if (!gameOver) {
			score++; 
			scoreText.text = "Score: " + score.ToString(); 
		}
	}
	/*
	public void ShowRewardedVideo ()
	{
		if (adButton.interactable) {
			ShowOptions options = new ShowOptions ();
			options.resultCallback = HandleShowResult;

			Advertisement.Show ("rewardedVideo", options);
		}
	}

	void HandleShowResult (ShowResult result)
	{
		if(result == ShowResult.Finished) {
			Debug.Log("Video completed - Offer a reward to the player");
			int hearts = PlayerPrefs.GetInt ("Hearts"); 
			hearts++;
			PlayerPrefs.SetInt ("Hearts", hearts); 
			heartText.text = hearts.ToString ();
			adButton.interactable = false;   
			restartFromHereText.text = "restart from here!"; 
			restartFromHereText.transform.localPosition = new Vector3 (0, 59.4F, 0); 

		}else if(result == ShowResult.Skipped) {
			Debug.LogWarning("Video was skipped - Do NOT reward the player");

		}else if(result == ShowResult.Failed) {
			Debug.LogError("Video failed to show");
		}
	}

	public void ReAnimatePoop() {

		Bird.Instance.ReAnimateBird (); 
		gameOverText.SetActive (false); 
		gameOver = false; 
		scoreText.gameObject.SetActive (true); 
	}

	public void NoAds() {
		playAds = false; 
	}

	public void UpdatePlayerHearts() {
		heartText.text = PlayerPrefs.GetInt ("Hearts").ToString(); 
	}*/
} 
