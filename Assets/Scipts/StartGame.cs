﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems; 
using UnityEngine.SceneManagement; 

public class StartGame : MonoBehaviour, IPointerUpHandler {

	public float waitBeforeBegin;

	public void OnPointerUp(PointerEventData eventData) {
		StartCoroutine (StartGameNow ()); 
	}

	IEnumerator StartGameNow() {
		if (AudioSourceScript.Instance.Playing) {
			AudioSourceScript.Instance.PlaySound (0); 
		}
		yield return new WaitForSeconds (waitBeforeBegin); 
		SceneManager.LoadScene (1); 
	}

}
