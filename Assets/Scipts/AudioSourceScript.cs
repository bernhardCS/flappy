﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioSourceScript : MonoBehaviour {

	private bool playing = true;  
	private static AudioSourceScript instance; 

	public AudioSource[] audioFiles; 


	public static AudioSourceScript Instance {
		get{ return instance; }
	}

	// Use this for initialization
	void Awake () {
		if (instance == null) {
			instance = this; 
		} else if (instance != null) {
			Destroy (gameObject); 
			return; 
		}
		DontDestroyOnLoad (gameObject); 
	}

	public void PlayPause(bool playing) {
		if (playing) {
			gameObject.GetComponent<AudioSource> ().Play (); 
			Playing = true; 
		} else {
			gameObject.GetComponent<AudioSource> ().Pause (); 
			Playing = false;
		}
	}

	public bool Playing {
		get{return playing; }
		set{ playing = value; }
	}

	public void PlaySound(int number) {
		audioFiles [number].Play (); 

	}

}
