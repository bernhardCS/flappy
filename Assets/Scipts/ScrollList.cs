﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScrollList : MonoBehaviour {
	// Use this for initialization

	private static ScrollList instance5;

	public static ScrollList Instance
	{
		get { return instance5; }
	}
	void Awake() {
		
		// If no Player ever existed, we are it.
		if (instance5 == null)
			instance5 = this;
		// If one already exist, it's because it came from another level.
		else if (instance5 != this) {
			Destroy (gameObject);
			return;
		}
		deviceID = SystemInfo.deviceUniqueIdentifier; 
	}
	//____________________________________________________________

	public GameObject ScrollEntry;
	public GameObject ScrollContain;
	public int yourPosition;
	public GameObject LoadingText; 
	public bool loading = true;


	private string deviceID; 

	void Update () {

		if (!loading) {
			LoadingText.SetActive (false);
		}  else {
			LoadingText.SetActive (true);
		}
	}

	public void getScrollEntrys()
	{
		//Destroy Objects that exists, because of a possible Call bevore
		foreach (Transform childTransform in ScrollContain.transform) Destroy(childTransform.gameObject);

		int j = 1;
		for (int i=0; i<HSController.Instance.onlineHighscore.Length-1; i++) {
			GameObject ScorePanel;
			ScorePanel = Instantiate (ScrollEntry) as GameObject;
			ScorePanel.transform.parent = ScrollContain.transform;
			ScorePanel.transform.localScale = ScrollContain.transform.localScale;
			Transform ThisScoreName = ScorePanel.transform.Find ("ScoreText");
			Text ScoreName = ThisScoreName.GetComponent<Text> ();
			//
			Transform ThisScorePoints = ScorePanel.transform.Find ("ScorePoints");
			Text ScorePoints = ThisScorePoints.GetComponent<Text> ();
			//
			Transform ThisScorePosition = ScorePanel.transform.Find ("ScorePosition");
			Text ScorePosition = ThisScorePosition.GetComponent<Text> ();

			ScorePosition.text = j+". ";
			string helpString = "";

			helpString = helpString+HSController.Instance.onlineHighscore [i]+"  ";
			i++;
			ScoreName.text = helpString;
			ScorePoints.text = HSController.Instance.onlineHighscore [i];
			i++;
			string onlineDeviceID = HSController.Instance.onlineHighscore [i];
			onlineDeviceID = onlineDeviceID.Substring (0, onlineDeviceID.IndexOf ('!')); 

			Debug.Log ("online DeviceID" + onlineDeviceID);

			if (onlineDeviceID == deviceID) {//player himself
				ScoreName.color=Color.magenta;
				ScorePoints.color=Color.magenta;
				ScorePosition.color=Color.magenta; 
			}  else if (j==1) { //position 1 
				ScoreName.color=Color.yellow;
				ScorePoints.color=Color.yellow;
				ScorePosition.color=Color.yellow;
			}  else if (j==300) {//last place
				ScoreName.color=Color.red;
				ScorePoints.color=Color.red;
				ScorePosition.color = Color.red;
			}
			j++;
		}
	}
}

