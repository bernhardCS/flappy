﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TapIcon : MonoBehaviour{

	private bool getSmaller = true; 

	// Update is called once per frame
	void Update () {
		if (getSmaller) {
			gameObject.transform.localScale *= 0.99F; 
			if (gameObject.transform.localScale.x < 0.6F) {
				getSmaller = false; 
			}
		} else {
			gameObject.transform.localScale /= 0.99F;
			if (gameObject.transform.localScale.x > 0.9F) {
				getSmaller = true;
			}
		}
	}
}
