﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems; 

public class ToHighscore : MonoBehaviour, IPointerUpHandler{

	public Canvas menuCanvas; 
	public Canvas highScoreCanvas; 

	public void OnPointerUp(PointerEventData eventData)  {
		if (AudioSourceScript.Instance.Playing) {
			AudioSourceScript.Instance.PlaySound (3); 
		}

		menuCanvas.gameObject.SetActive (false); 
		highScoreCanvas.gameObject.SetActive (true);

		HSController.Instance.startGetScores (); 

	}
}
