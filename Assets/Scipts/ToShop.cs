﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems; 
using UnityEngine.UI; 

public class ToShop : MonoBehaviour, IPointerUpHandler {

	public Button noAds; 
	public Text noAdsText; 
	public Text currentHeartsNumber; 

	public void OnPointerUp(PointerEventData eventData) {
		if (AudioSourceScript.Instance.Playing) {
			AudioSourceScript.Instance.PlaySound (3); 
		}

		bool notInteractable = PlayerPrefs.GetString ("Ads").Equals ("No");
		noAds.interactable = !notInteractable;
		if (notInteractable) {
			noAdsText.GetComponent<Text> ().color = Color.grey;
		}
		currentHeartsNumber.text = PlayerPrefs.GetInt ("Hearts").ToString(); 

	}

}
